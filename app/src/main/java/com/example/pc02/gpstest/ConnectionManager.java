package com.example.pc02.gpstest;

import android.bluetooth.BluetoothSocket;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by PC02 on 24.04.2017.
 */

public class ConnectionManager extends Thread { //В этом классе обрабатывается текущее Bluetooth  соеденение
    final static String MY_TAG = "MyLog";
    final static int CONNECTION_START = 0;
    final static int CONNECTION_STOP = 1;
    final static int READ_MESSAGE = 2;
    final static int WRITE_MESSAGE = 3;


    private ObjectInputStream in;
    private ObjectOutputStream out;
    private BluetoothSocket socket;
    private Location location;
    private Handler handler;
    public ConnectionManager(BluetoothSocket s,Handler h){
        handler = h;
        socket = s;
        BufferedInputStream inTmp = null;
        try {
            inTmp = new BufferedInputStream(socket.getInputStream());
        } catch (Exception e) {
        }
        try {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            in = new ObjectInputStream(inTmp);
            Log.e(MY_TAG,"ConnectionManager InputStream получен");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }//Конструктор вызываемый сервером, для чтения

    public ConnectionManager(BluetoothSocket s,Handler h,Location l){
        location = l;
        socket = s;
        handler = h;
        BufferedOutputStream outTmp= null;
        try {
            outTmp = new BufferedOutputStream(socket.getOutputStream());
        } catch (Exception e) {

        }
        try {
            out = new ObjectOutputStream(outTmp);
            out.flush();
            Log.e(MY_TAG,"ConnectionManager OutputStream получен");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }//Конструктор вызываемый клиентом, для записи

    public void run(){
        LatLng result = null;
        handler.sendEmptyMessage(CONNECTION_START);
        if (location!=null){
            write(location);
        }else{
            result = read();
            Message msg = handler.obtainMessage(READ_MESSAGE,result);
            msg.sendToTarget();
        }

    }

    public LatLng read(){
        LatLng result = null;
        double latitude=0;
        double longitude=0;

        while (result==null){
            try {
                latitude = in.readDouble();
                longitude = in.readDouble();
                result = new LatLng(latitude,longitude);
                in.close();
                socket.close();
            } catch (Exception e) {
                String s = e.getMessage();

            }
        }
        return  result;
    }

    public void write(Location location){
        try {
            out.writeDouble(location.getLatitude());
            out.flush();
            out.writeDouble(location.getLongitude());
            out.flush();
            socket.close();
            out.close();
            handler.sendEmptyMessage(WRITE_MESSAGE);
        } catch (IOException e) {
            Log.e(MY_TAG,e.getMessage());
        }
    }
}
