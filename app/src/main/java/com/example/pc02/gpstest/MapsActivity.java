package com.example.pc02.gpstest;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ShowableListMenu;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.Date;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private Button Locate;
    private Button Send;
    private LocationManager locationManager;
    Location location;
    final static String MY_TAG = "MyLog";
    private SupportMapFragment mapFragment;
    private BluetoothServer server= null;
    private BluetoothClient client;
    BluetoothDevice device;
    LatLng coordinates=null;
    private Handler handler;
    private int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this); // Инициализация компонентов
        Locate = (Button) findViewById(R.id.locate);
        Locate.setOnClickListener(this);
        Send = (Button) findViewById(R.id.send);
        Send.setOnClickListener(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        EnableBluetooth(); //Включаем Bluetooth
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            ShowGPSDialog(); //Проверяем GPS, если выключен перенаправяляем пользователя в настройки
            return;
        }
        handler = new Handler() {
            public void handleMessage(android.os.Message msg) { //Инициализируем handler, для отправки данных из дочерних потоков в UI;
                switch (msg.what) {
                    case ConnectionManager.CONNECTION_START:
                        Log.e(MY_TAG, "Соеденение установлено");
                        break;
                    case ConnectionManager.CONNECTION_STOP:
                        Log.e(MY_TAG, "Соеденение разорвано");
                        break;
                    case ConnectionManager.READ_MESSAGE:
                        AddMarker((LatLng) msg.obj);
                        if (!server.isAlive()) {
                            server = new BluetoothServer(BluetoothAdapter.getDefaultAdapter(), handler);
                            server.start();
                        }
                        break;
                    case ConnectionManager.WRITE_MESSAGE:
                        Log.e(MY_TAG, "Координаты отправлены");
                        if (!server.isAlive()) {
                            server = new BluetoothServer(BluetoothAdapter.getDefaultAdapter(), handler);
                            server.start();
                        }

                        break;
                    default:
                        break;
                }
            }
        };
        server = new BluetoothServer(BluetoothAdapter.getDefaultAdapter(), handler);//Запускаем сервер
        server.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null && resultCode != Activity.RESULT_OK) {
            return;
        }
        device = data.getParcelableExtra("device"); //Получаем BluetoothDevice, которое пользователь выбрал из списка
        server.cancel();//Закрываем сервер, т.к. будет создан клиент, который подключится к выбранному устройству
        flag = 1;// флаг==1 говорит о том, что после обновления местоположения необходимо отправить текущие координаты другому устройству
        UpdateLocation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RemoveUpdate();
        if (server.isAlive()) {
            server.cancel();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.locate:
                flag = 0;//флаг==0 говорит о том, что после обновления текущего местаположения необходимо создать маркер на карте
                UpdateLocation();
                break;
            case R.id.send:
                Intent intent = new Intent(this, ListDevices.class);
                startActivityForResult(intent, 1);
            default:
                break;
        }
    }

    private void UpdateLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        } else {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);//Получаем последнее известное местоположение
        }
        try {
            Date date = new Date();
            long time = date.getTime();//Получаем текущее время
            if (time - location.getTime() < 60 * 5 * 1000) {//Если последнее местаположение обновлено менн 5 мин. отправляем его
                if (flag == 1) {
                    client = new BluetoothClient(device, handler, location);
                    client.start();
                } else {
                    AddMarker();
                }
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }

        } catch (Exception e) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }

    private void AddMarker() { //Добавляем маркер с текущим местаположением
        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(myLocation)
                .zoom(15)
                .bearing(45)
                .tilt(20)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(cameraUpdate);

    }

    private void AddMarker(LatLng coordinate) {//Создаем маркер по полученым координатам
        Log.e(MY_TAG, Double.toString(coordinate.latitude));
        Log.e(MY_TAG, Double.toString(coordinate.longitude));
        mMap.addMarker(new MarkerOptions().position(coordinate));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(coordinate)
                .zoom(15)
                .bearing(45)
                .tilt(20)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(cameraUpdate);
    }

    private void EnableBluetooth() {
        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);
    }

    private void RemoveUpdate() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }else{
            locationManager.removeUpdates(locationListener);
        }

    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location loc) {
            location = loc;
            if (flag == 1) {
                client = new BluetoothClient(device, handler, location);
                client.start();
            } else {
                AddMarker();
            }
            RemoveUpdate();

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private void ShowGPSDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("GPS выключен");
                builder.setMessage("Перейти в настройки?");
                builder.setPositiveButton("НАСТРОЙКИ",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        });
                builder.setNegativeButton("ОТМЕНА", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
