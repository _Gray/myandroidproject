package com.example.pc02.gpstest;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Handler;

/**
 * Created by PC02 on 24.04.2017.
 */

public class BluetoothServer extends Thread{ //Этот класс реализует сервер, ожидающий подключения клиента
    final static String MY_TAG = "MyLog";
    private final BluetoothServerSocket mServerSocket;
    private final String NAME_SERVER = "Test Bluetooth";
    private final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ConnectionManager connectionManager;
    private android.os.Handler handler;

    public BluetoothServer(BluetoothAdapter mBluetoothAdapter, android.os.Handler h){

        BluetoothServerSocket tmp=null;
        handler = h;
        try {
            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME_SERVER, uuid);
            Log.e(MY_TAG,"BluetoothServer серверный сокет получен");
        } catch (IOException e) {
        }
        mServerSocket = tmp;
    }

    public void run (){
        BluetoothSocket socket = null;
        Log.e(MY_TAG,"BluetoothServer сервер запущен");
        while (true){
            try{
                socket = mServerSocket.accept();
                connectionManager = new ConnectionManager(socket,handler);//Передаем подключенный сокет в другой поток и закрываем сервер
                connectionManager.start();
                cancel();
            }catch (IOException e){
                Log.e("MyLog","BluetoothServer сервер закрыт");
                break;
            }
        }

    }

    public void cancel ()
    {
        try {
            mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
