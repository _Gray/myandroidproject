package com.example.pc02.gpstest;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListDevices extends Activity implements View.OnClickListener{

    BroadcastReceiver br;
    BluetoothAdapter ba;
    final static String MY_TAG = "MyLog";
    final ArrayList<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();
    final ArrayList<String> names = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_devices);

        Button Update = (Button)findViewById(R.id.update);
        ListView listView = (ListView)findViewById(R.id.ListFind);
        ba = BluetoothAdapter.getDefaultAdapter();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names);


        br = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    BluetoothDevice buf = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if(buf.getName()!=null)
                    {
                        devices.add(buf);
                        names.add(buf.getName());
                        adapter.notifyDataSetChanged();
                    }
                }
            }

        };
        IntentFilter filter=new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(br, filter);


        Update.setOnClickListener(this);
        listView.setAdapter(adapter);
        ba.startDiscovery();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                BluetoothDevice device = devices.get(position);
                ba.cancelDiscovery();
                unregisterReceiver(br);
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                SendToActivity(device);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        ba.cancelDiscovery();
        try {
            unregisterReceiver(br);
        }catch (Exception e)
        {

        }
    }

    @Override
    public void onClick(View view)
    {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void SendToActivity(BluetoothDevice device){
        Intent intent = new Intent(this,MapsActivity.class);
        intent.putExtra("device",device);
        setResult(Activity.RESULT_OK,intent);
    }
}
