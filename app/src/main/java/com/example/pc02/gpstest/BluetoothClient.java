package com.example.pc02.gpstest;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.logging.Handler;

/**
 * Created by PC02 on 24.04.2017.
 */

public class BluetoothClient extends Thread {
    final static String MY_TAG = "MyLog";

    private final BluetoothSocket socket;
    private  final BluetoothDevice device;
    private final UUID MyUUID= UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ConnectionManager connectionManager;
    private Location location;
    private android.os.Handler handler;

    public BluetoothClient (BluetoothDevice d, android.os.Handler h, Location l){
        handler = h;
        location =l;
        device = d;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        BluetoothSocket tmp = null;
        try {
            tmp = device.createRfcommSocketToServiceRecord(MyUUID);
            Log.e(MY_TAG,"BluetoothClient сокет получен");
        }catch(IOException e){
        }
        socket = tmp;
    }

    public void run (){

        try {

            socket.connect();
            Log.e(MY_TAG,"BluetoothClient Клиент запущен");
            connectionManager = new ConnectionManager(socket,handler,location);//Передаем подключенный сокет в другой поток
            connectionManager.start();
        } catch (IOException e) {
            Log.e(MY_TAG,e.getMessage());
        }



    }
}
